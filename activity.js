db.fruits.aggregate([
    {
        $match: {
            supplier: "Red Farms"
        }
    },
    {
        $count: "totalItems"
    }
])

db.fruits.aggregate([
    {
        $match: {
            price: {
                $gt: 50
            }
        }
    },
    {
        $count: "totalItems"
    }
])

db.fruits.aggregate([
    {
        $match: {
            onSale: true
        }
    },
    {
        $group: {
            _id: "$supplier",
            avgPrice: {
                $avg: "$price"
            }
        }
    }
])

db.fruits.aggregate([
    {
        $match: {
            onSale: true
        }
    },
    {
        $group: {
            _id: "$supplier",
            highestPrice: {
                $max: "$price"
            }
        }
    }
])

db.fruits.aggregate([
    {
        $match: {
            onSale: true
        }
    },
    {
        $group: {
            _id: "$supplier",
            lowestPrice: {
                $min: "$price"
            }
        }
    }
])


